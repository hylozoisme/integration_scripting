import datetime
import sys
import locale

well_said = {"fr_FR": "Bien dit !", "en_US": "Well said !"}
hello = {"fr_FR": "Bonjour !", "en_US": "Hello !"}
good_afternoon = {"fr_FR": "Bon après-midi !", "en_US": "Good afternoon !"}
good_evening = {"fr_FR": "Bonsoir !", "en_US": "Good evening !"}


def polite_shell(language, now):
    """
    :param language:
    :param now:

    - Dit bonjour/ bon après midi / bonsoir selon le moment de la journée et dans le langage de l'utilisateur
    """
    if now.hour < 12:
        print(hello[language])
    elif now.hour < 18:
        print(good_afternoon[language])
    else:
        print(good_evening[language])


def ohce():
    """
        *** Shell application ***
        - Say hello/good afternoon/good evening according to the time of day and in the user's language
        - Return the user's input in mirror
        - If the user's input is a palindrome, say "Well said !"
        - At the end, say hello/good afternoon/good evening according to the time of day and in the user's language
    """
    now = datetime.datetime.now()
    language = locale.getdefaultlocale()[0]

    polite_shell(language, now)
    word = input("Waiting for user input... \n")
    print(word[::-1])
    if word == word[::-1]:
        print(well_said[language])

    polite_shell(language, now)


ohce()
