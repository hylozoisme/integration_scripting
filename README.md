### OHCE

**Shell application**
- Say hello/good afternoon/good evening according to the time of day and in the user's language **(fr_FR and en_EN only)**
- Return the user's input in mirror
- If the user's input is a palindrome, say "Well said !"
- At the end, say hello/good afternoon/good evening according to the time of day and in the user's language